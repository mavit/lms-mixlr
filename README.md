# `lms-mixlr`

`lms-mixlr` is a [Mixlr](https://mixlr.com/) plugin for [Lyrion Music Server](https://lyrion.org/).  You can use it to play Mixlr broadcasts though on your Squeezebox network music player.

## Installation

Browse to Settings → Plugins → Additional Repositories, and add `https://mavit.gitlab.io/lms-mixlr/extensions.xml` to the list.

Version 8 of Lyrion Music Server is required.

## Usage

This plugin is currently extremely basic.

- To play a broadcast from Mixlr, paste the URL of the Mixlr webpage into Lyrion Music Server’s Tune In URL field in the Radio section of the web interface.

That’s it!

## Licence

Copyright 2020-2021, 2023-2024 Peter Oliver.

`lms-mixlr` is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

`lms-mixlr` is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with `lms-mixlr`.  If not, see <https://www.gnu.org/licenses/>.
