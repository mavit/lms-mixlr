package Plugins::Mixlr::Plugin;

# Copyright 2020, 2023 Peter Oliver.

# This file is part of lms-mixlr.
#
# lms-mixlr is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# lms-mixlr is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with lms-mixlr.  If not, see <https://www.gnu.org/licenses/>.

use strict;
use base 'Slim::Plugin::Base';

# use Slim::Utils::Strings;
use JSON::XS::VersionOneAndTwo;
use URI;

use constant 'PAGE_URL_REGEXP' => qr{
	                                    ^ http s? ://
	                                    (?! (?:api|listen|www) \.)
	                                    (?: ([^/?#]+) \.)?
	                                    mixlr\.com/
}x;

my $log = Slim::Utils::Log->addLogCategory({
	'category'     => 'plugin.mixlr',
	'defaultLevel' => 'ERROR',
	'description'  => 'PLUGINS_MIXLR',
});

sub initPlugin {
	my $class = shift;
	$class->SUPER::initPlugin();

	Slim::Player::ProtocolHandlers->registerURLHandler(
		PAGE_URL_REGEXP() => 'Plugins::Mixlr::Plugin'
	);
}

sub explodePlaylist {
	my ($class, $client, $uri, $callback) = @_;

	my $mixlr_user = do {
		if ( $uri =~ PAGE_URL_REGEXP() ) {
			$1 // (URI->new($uri)->path_segments)[1];
		}
	};

	if ( defined($mixlr_user) ) {
		my $user_url = URI->new('https://api.mixlr.com/');
		$user_url->path_segments('users', $mixlr_user);

		Slim::Networking::SimpleAsyncHTTP->new(
			sub {
				my $http = shift;
				$callback->([
					map {
						'https://listen.mixlr.com/'. $_
					} @{from_json($http->content())->{'broadcast_ids'} // []}
				]);
			},
			sub {
				my ($http, $error) = @_;
				$log->error($error);
				$callback->([]);
			},
		)->get($user_url->as_string);
	}
	else {
		$callback->([]);
	}
}

1;
